#ifndef __dbg_h__
#define __dbg_h__

#include <stdio.h>
#include <errno.h>
#include <string.h>

#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RST "\x1B[0m"

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...) fprintf(stderr, MAG "[DEBUG]" RST " (%s:%s:%d) " M "\n", \
			      __func__, __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_err(M, ...) fprintf(stderr,                                                                           \
				RED "[ERROR]" RST " (%s:%s:%d: errno: %s) " M "\n", __func__, __FILE__, __LINE__, \
				clean_errno(), ##__VA_ARGS__)

#define log_warn(M, ...) fprintf(stderr,                                            \
				 YEL "[WARN]" RST " (%s:%s:%d: errno: %s) " M "\n", \
				 __func__, __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)

#define log_info(M, ...) fprintf(stderr, "[INFO] (%s:%s:%d) " M "\n", \
				 __func__, __FILE__, __LINE__, ##__VA_ARGS__)

#define check(A, M, ...)                   \
	if (!(A)) {                        \
		log_err(M, ##__VA_ARGS__); \
		errno = 0;                 \
		goto error;                \
	}

#define sentinel(M, ...)                   \
	{                                  \
		log_err(M, ##__VA_ARGS__); \
		errno = 0;                 \
		goto error;                \
	}

#define check_mem(A) check((A), "Out of memory.")

#define check_debug(A, M, ...)           \
	if (!(A)) {                      \
		debug(M, ##__VA_ARGS__); \
		errno = 0;               \
		goto error;              \
	}
#endif

#ifndef _babel_controller_h
#define _babel_controller_h

#include <SDL2/SDL.h>

std::vector<SDL_GameController> get_connected_controllers;

void remove_controller(int id);

#endif

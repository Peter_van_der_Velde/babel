#ifndef _babel_physical_device_h
#define _babel_physical_device_h

#include <vector>
#include <vulkan/vulkan.h>

#include "common_structs.h"

bool isDeviceSuitable(VkPhysicalDevice device);
void pickPhysicalDevice();
SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);

QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);
bool checkDeviceExtensionSupport(VkPhysicalDevice device);
std::vector<const char*> getRequiredExtensions();

#endif

#ifndef _babel_global_h
#define _babel_global_h

#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

extern SDL_Window* sdl_window;

const std::vector<const char*> validationLayers = {
	"VK_LAYER_KHRONOS_validation"
};

extern VkInstance instance;
extern VkPhysicalDevice physicalDevice;
extern VkDevice device;

extern VkQueue graphicsQueue;
extern VkQueue presentQueue;

extern VkSurfaceKHR surface;
extern const std::vector<const char*> deviceExtensions;

#endif
